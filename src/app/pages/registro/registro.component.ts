import { NgForOf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdmUser } from '../../models/user.model';
import { AutenticationService } from '../../services/autentication.service';

 
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  user: AdmUser;
  constructor(private auth: AutenticationService) {
    this.user = new AdmUser();
   }

  ngOnInit() { }
    
  createAccount(dataForm: NgForm){
    if (dataForm.invalid){return;}
    this.auth.signUp(this.user).subscribe(res=>{
      console.log(res);
    }, (err)=>{
      console.log(err.error.error.message);
    });
  }

}
