import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdmUser } from '../../models/user.model';
import { AutenticationService } from '../../services/autentication.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: AdmUser;

  constructor(private auth: AutenticationService) { 
    this.user =  new AdmUser();
  }

  ngOnInit() {
  }

  login(dataLoginForm: NgForm){
      if(dataLoginForm.invalid){return;}
      this.auth.login(this.user).subscribe(resp=>{
        console.log(resp);
      },(err)=>{
        console.log(err.error.error.message);
      })
  }

}
