import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { AdmUser } from '../models/user.model';
import {map} from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class AutenticationService {

   URL = 'https://identitytoolkit.googleapis.com/v1/accounts:';
   API_KEY = 'AIzaSyCWrE1juFJSgv-gNLhdVbwsRXTvl-ZFdCI';
   userToken = '';

    

  //  https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  //m https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  constructor(private http: HttpClient) { }

  signUp(user: AdmUser){

    const userData = {
      // ... user jala todos las propiedades de user y los pasa a este modelo
      ...user,
      returnSecureToken : true,
    }

    return this.http.post(`${this.URL}signUp?key=${this.API_KEY}`, userData).pipe(
      map(resp=>{
        this.saveToken(resp['idToken']);
        return resp;
      })
    );
  }

  login(user: AdmUser){
    const userData = {
      // ... user jala todos las propiedades de user y los pasa a este modelo
      ...user,
      returnSecureToken : true,
    }

    return this.http.post(`${this.URL}signInWithPassword?key=${this.API_KEY}`, userData).pipe(
      map(resp=>{
        this.saveToken(resp['idToken']);
        return resp;
      })
    );
  }

  logOut(){

  }

  private saveToken(idToken: string){
    this.userToken = idToken;
    localStorage.setItem('token', idToken); }

  private getToken(){
    return this.userToken = (localStorage.getItem('token'))? localStorage.getItem('token'): '';
  }
}
